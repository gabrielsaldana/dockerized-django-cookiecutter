# Dockerized Django Cookiecutter

This is a template for a basic Django setup with Docker

Requirements:
- python
- cookiecutter
- pre-commit
- docker

To use, simply run:

``` shell
cookiecutter <project_slug>
```

Replace `<project_slug>` with your project name without special characters and replacing spaces with underscores `_`.
