#!/usr/bin/env bash
set -eux

cd /opt/{{ cookiecutter.project_slug }}/source;

echo "Migrating..."
python ./manage.py migrate

echo "Migration complete."
# Collect static files
python ./manage.py collectstatic --no-input

echo "Starting up web server..."
gunicorn --workers=${GUNICORN_NUM_WORKERS} --worker-class=${GUNICORN_CLASS} --bind=0.0.0.0:8000 {{ cookiecutter.project_slug }}.wsgi
