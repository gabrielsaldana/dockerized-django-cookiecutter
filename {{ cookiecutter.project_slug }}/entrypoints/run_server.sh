#!/usr/bin/env bash
set -euo pipefail

echo "Migrating..."
python ./source/manage.py migrate
echo "Migration complete."
echo "Starting up dev server..."
exec python ./source/manage.py runserver_plus --keep-meta-shutdown 0.0.0.0:8000
