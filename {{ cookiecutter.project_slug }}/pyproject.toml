[tool.poetry]
name = "{{ cookiecutter.project_slug }}"
version = "{{ cookiecutter.project_version }}"
description = ""
authors = ["{{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>"]

[tool.poetry.dependencies]
python = "^3.11"
Django = "^5.0.6"
django-extensions = "^3.1.5"
django-environ = "^0.11.2"
psycopg2-binary = "^2.9.3"
djangorestframework = "^3.13.1"
drf-spectacular = "^0.27.2"

[tool.poetry.dev-dependencies]
pytest = "^8.2.2"
pytest-django = "^4.5.2"
pytest-cov = "^5.0.0"
pytest-factoryboy = "^2.3.1"
pytest-mock = "^3.7.0"
pytest-html = "^4.1.1"
Werkzeug = "^3.0.3"
django-coverage-plugin = "^3.1.0"
ruff = "^0.5.1"

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.isort]
py_version = 311
force_grid_wrap = 0
profile = "black"
include_trailing_comma = true
line_length = 120
multi_line_output = 3
use_parentheses = true
ensure_newline_before_comments = true

[tool.ruff]
target-version = "py311"
extend-exclude = [
    "*/migrations/*",
]
src = ["."]
line-length = 120

[tool.ruff.lint]
select = ["E", "F", "W", "B", "I", "S", "DJ", "C90", "UP", "PL", "RUF"]
ignore = [
    "E501",  # line too long, handled by black
    "B008",  # do not perform function calls in argument defaults
    # Recommendations from Ruff docs https://docs.astral.sh/ruff/formatter/#conflicting-lint-rules
    "W191",
    "E111",
    "E114",
    "E117",
    "D206",
    "D300",
    "Q000",
    "Q001",
    "Q002",
    "Q003",
]

[tool.ruff.lint.per-file-ignores]
"*tests/*" = ["S101"]

[tool.coverage.run]
branch = true
omit = [
  "*apps.py",
  "*migrations/*",
  "*settings*",
  "*tests/*",
  "*urls.py",
  "*wsgi.py",
  "*asgi.py",
  "*manage.py",
  "*__init__.py",
  "*.html",
]
plugins = ["django_coverage_plugin"]
source = ["."]

[tool.mypy]
python_version = "3.11"
mypy_path = "./source/"
disallow_untyped_defs = true
check_untyped_defs = true
disallow_incomplete_defs = true
disallow_any_generics = true
disallow_untyped_calls = true
disallow_untyped_decorators = false
ignore_errors = false
ignore_missing_imports = true
implicit_reexport = false
strict_optional = true
strict_equality = true
no_implicit_optional = true
warn_unused_ignores = true
warn_redundant_casts = true
warn_unused_configs = true
warn_unreachable = true
warn_no_return = true

[[tool.mypy.overrides]]
module = "manage"
ignore_errors = true

[[tool.mypy.overrides]]
module = "*.migrations.*"
ignore_errors = true

[[tool.mypy.overrides]]
module = "*.tests.*"
ignore_errors = true

[tool.pytest.ini_options]
DJANGO_SETTINGS_MODULE = "{{ cookiecutter.project_slug }}.settings"
python_files = ["tests.py", "test_*.py"]
mock_use_standalone_module = true
norecursedirs = [
    "{{ cookiecutter.project_slug }}",
    "hooks",
    "*.egg",
    ".eggs",
    "dist",
    "build",
    "docs",
    ".tox",
    ".git",
    "__pycache__",
    "htmlcov"
]
addopts = """--color=yes
    -p no:warnings
    --disable-pytest-warnings
    --self-contained-html
    --html=reports/unittests/test_report.html
    --junitxml=reports/unittests/junit_report.xml
    --cov=source
    --cov-fail-under=90
    --cov-report=term-missing:skip-covered
    --cov-report=html:reports/coverage/htmlcov
    --cov-report=xml
    --cov-branch"""
