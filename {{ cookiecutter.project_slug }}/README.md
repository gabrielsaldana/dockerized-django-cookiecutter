# {{ cookiecutter.project_name }}

{{ cookiecutter.project_description }}

## Requirements

- Docker

All project dependencies are contained in Docker containers and using docker-compose.

## Setup

To run locally, run:

``` shell
make start
```

## Development

Read [CONTRIBUTING](CONTRIBUTING.md)

## Deployments

Add here the steps to deploy
